﻿using MediatR;
using System.Threading.Tasks;
using System.Threading;
using System;
using WebAPI_project_banhang.Modules.M_User.Models;
using WebAPI_project_banhang.Modules.M_User.Repositories;
using WebAPI_project_banhang.Modules.M_User.ViewModels;
using WebAPI_project_banhang.Modules.M_Users.ViewModels;

namespace WebAPI_project_banhang.Modules.M_Users.Queries
{
    public class LoginQuery : IRequest<string>
    {
        public LoginViewModel _loginViewModel { get; set; }

        public LoginQuery(LoginViewModel loginViewModel)
        {
            _loginViewModel = loginViewModel;
        }
    }
    public class LoginQueryHandler : IRequestHandler<LoginQuery, string>
    {
        private readonly IUserRepository _userRepository;

        public LoginQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<string> Handle(LoginQuery request, CancellationToken cancellationToken)
        {
            return await _userRepository.LoginUser(request._loginViewModel);
        }
    }
}
